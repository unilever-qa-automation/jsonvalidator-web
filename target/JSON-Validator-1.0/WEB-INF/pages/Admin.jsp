<%-- 
    Document   : URL
    Created on : 20 Jan, 2015, 10:58:17 PM
    Author     : Sachin
--%>

<!DOCTYPE html>
<html lang="en">
    <%@include file="/WEB-INF/jspf/isUserAuthenticated.jspf"%>
    <%@include file="/WEB-INF/jspf/AccessingAdminContents.jspf"%>
    <head> 
        <%@include file="/WEB-INF/jspf/head.jspf"%>
        <link href="${pageContext.request.contextPath}/assets/css/admin.css" rel="stylesheet">        
        <title>Admin Panel</title>        
    </head>
    <body role="document">    
        <div class="container-fluid">
            <div class="row">                
                <div class="col-md-2 sidebar" id="sidebar">                  
                    <%@include file="/WEB-INF/jspf/sidebar.jspf"%>
                </div>
                <div class="col-md-10" id="main1">                                 
                    <div class="" id="main2">
                        <div class="row">                            
                            <div class="" id="sachin">                            
                                <div class="tool-header">
                                    <h1>Administrator Panel</h1>
                                    <h4>Control & Modify.....</h4>
                                </div>
                            </div>                            
                            <div id="content">                               
                                <div class="alert alert-danger alert-dismissible fade in hidden" role="alert" style="width:70%;margin:0 auto;"> <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">�</span></button> <h4>Oh snap! You got an error! <br/>Unable to load Schemas</h4> <p id='msg'></p> </div>
                                <div class="section">Admin Panel:</div>
                                <div class="section-data my-row"> 
                                    <div id="tabs1" class=''> 
                                        <ul class="nav nav-tabs">
                                            <li class="active"><a href="#template-tab" data-toggle="tab">JSON Schemas<i class="fa"></i></a></li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="template-tab">
                                                <div class="tab-pane-data">
                                                    <div class="section-data my-row" id="configure-template">
                                                        <a href="javascript:void(0)" id ='addTemp-button' class="btn btn-primary pull-right"><i class="fa fa-plus"></i>&ensp; Add JSON Schemas</a>
                                                        <table id='template-table' class="table table-bordered table-condensed table-striped">
                                                            <thead>
                                                                <tr>
                                                                    <td>SN</td>
                                                                    <td>Schema Branch Name</td>
                                                                    <td>Delete</td>
                                                                </tr>
                                                            </thead>
                                                            <tbody>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                                    
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="addTemp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h3 class="modal-title">Add New JSON Schemas</h3>
                    </div>
                    <div class="modal-body">
                        <div id="scema-info">Upload Zip containing all schemas as specified directory.<p>&nbsp;</p></div>
                        <form name="addTemp-form" id="addTemp-form" action='UploadSchema' method='post' enctype="multipart/form-data">
                            <div class="form-group">
                                <label for="id1" class=" control-label">Schema Branch Name:</label>
                                <div class="controls">
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-th-list"></i></span>
                                        <input type="text" class="form-control" name="id1" id="id1" placeholder="Schema Branch Name" title="Enter Schema Branch Name. It will be used for folder name" data-toggle="tooltip">

                                    </div> 
                                </div>
                            </div>  
                            <div class=" form-group">
                                <label for="fileURL" class="col-form-label">Schema's zip: </label>
                                <div class="form-section">
                                    <div class="form-group ">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-file" aria-hidden="true"></span>
                                            </span>                                        
                                            <input type="file" class="form-control " id="fileURL" name="fileURL" title ="Browse zip file containing schemas" data-toggle="tooltip">
                                        </div>
                                        <div class="hidden" id='bar'>
                                            <div class="progress" >
                                                <div id="orangeBar" class="progress-bar progress-bar-info progress-bar-striped progress-bar-animated active" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                                            </div> 
                                            <div id='status'><b>Status: </b><span></span></div>
                                        </div>
                                        <div id='msgs'></div>
                                        <div class='clearfix'></div>      
                                    </div>
                                </div>
                            </div> 
                            <div class="form-group">
                                <div class="">
                                    <p>&nbsp;</p>
                                    <button type="submit" class="btn btn-block btn-primary">Add </button>
                                </div>
                            </div>                            
                        </form>
                    </div>
                </div>
            </div>
        </div> 
        <%@include file="/WEB-INF/jspf/footerScripts.jspf"%>
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/jquery.fileupload.js"></script>
        <script type="text/javascript" src="${pageContext.request.contextPath}/assets/js/admin.js"></script>        
    </body>
</html>