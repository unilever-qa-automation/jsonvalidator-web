/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package json.validator.web;

import java.io.File;
import java.io.FilenameFilter;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import net.lingala.zip4j.core.ZipFile;

import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author sku202
 */
public class HelperUtils {

    /**
     * Method returns the unique string based on time stamp
     *
     *
     * @return unique string
     */
    public static String generateUniqueString() {
        DateFormat df = new SimpleDateFormat("dd-MMMM-yyyy");
        DateFormat df1 = new SimpleDateFormat("hh-mm-ss-SSaa");
        Calendar calobj = Calendar.getInstance();
        String time = df1.format(calobj.getTime());
        String date = df.format(calobj.getTime());
        return date + "_" + time;
    }

    public static void unzip(File file, File schemaPath) throws ZipException,Exception {
            ZipFile zipFile = new ZipFile(file);            
            zipFile.extractAll(schemaPath.getAbsolutePath());      
            System.out.println("SchemaPath: "+schemaPath.getAbsolutePath());
            System.out.println("File: "+file.getAbsolutePath());
            String[] names= schemaPath.list((File dir, String name) -> "schemas".equals(name)||"SchemaMapping.properties".equals(name));
            if(names.length!=2){
                FileUtils.deleteQuietly(schemaPath);
                throw new Exception("Zip structure is not correct. Its should contain schemas folder and SchemaMapping.properties file");
            }
    }

}
