/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package json.validator.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.JobWithDetails;
import com.offbytwo.jenkins.model.QueueItem;
import com.offbytwo.jenkins.model.QueueReference;

/**
 *
 * @author sku202
 */
public class Build extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        try (PrintWriter out = response.getWriter()) {
            JSONObject json = new JSONObject();
            String username = "", password = "", email = "";
            String SiteAddress = request.getParameter("siteURL");
            String schema = request.getParameter("schema");
            String SinglePage = ("Yes".equalsIgnoreCase(request.getParameter("CrawlSite"))) ? "No" : "Yes";
            String CrawlerConfigFile = request.getParameter("CrawlerConfigFile").isEmpty()?"":new File(request.getParameter("CrawlerConfigFile")).getAbsolutePath();
            if (request.getParameterMap().containsKey("setAuthentication")
                    && "on".equalsIgnoreCase(request.getParameter("setAuthentication"))) {
                username = request.getParameter("username");
                password = request.getParameter("password");
            }
            if (request.getParameterMap().containsKey("email")) {
                email = request.getParameter("email");
            }
            String ipAddress = request.getHeader("X-FORWARDED-FOR");
            if (ipAddress == null) {
                ipAddress = request.getRemoteAddr();
            }

            // build param
            Map<String, String> map = new HashMap<>();
            map.put("SiteAddress", SiteAddress);
            map.put("Username", username);
            map.put("Password", password);
            map.put("SinglePage", SinglePage);
            map.put("CrawlerConfigFileWeb", CrawlerConfigFile);
            map.put("email", email);
            map.put("Schema", schema);
            map.put("machine", ipAddress);
            map.put("interface", "web");
            
            // Jenkins Build
            json.put("builds", "http://10.207.16.9/jenkins/job/JSON-Validator/");
            JenkinsServer js = new JenkinsServer(URI.create("http://10.207.16.9/jenkins/"));
            JobWithDetails job = js.getJob("JSON-Validator");
            QueueReference queueRef = job.build(map);
            QueueItem queueItem = js.getQueueItem(queueRef);
            json.put("buildID", job.getNextBuildNumber() + js.getQueue().getItems().size() - 1);
            if (job.isInQueue()) {
                json.put("status", "inQueue");
                json.put("reason", queueItem.getWhy());
                json.put("url", queueItem.getUrl());
                if (queueItem.isCancelled()) {
                    json.put("status", "Canceled");
                }
            }
            Thread.sleep(2000);
            com.offbytwo.jenkins.model.Build build = js.getJob("JSON-Validator").getLastBuild();
            if (build.details().isBuilding()) {
                json.put("url", build.getUrl() + "/console");
                json.put("reason", "");
                json.put("status", "Executing");
            }
            out.print(json);
            out.close();
        } catch (Exception ex) {
            Logger.getLogger(Build.class.getName()).log(Level.SEVERE, null, ex);
            response.sendError(500, ex.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on
    // the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
