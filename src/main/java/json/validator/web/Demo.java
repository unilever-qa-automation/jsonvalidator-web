/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package json.validator.web;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.http.client.fluent.Form;
import org.apache.http.client.fluent.Request;
import org.json.JSONObject;

import com.offbytwo.jenkins.JenkinsServer;
import com.offbytwo.jenkins.model.JobWithDetails;
import com.offbytwo.jenkins.model.QueueItem;
import com.offbytwo.jenkins.model.QueueReference;

/**
 *
 * @author sku202
 */
public class Demo extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            JSONObject json = new JSONObject();
            JenkinsServer js = new JenkinsServer(URI.create("http://10.207.16.9/jenkins/"));
            JobWithDetails job = js.getJob("JSON-Validator");
            Map<String, String> map = new HashMap<>();
            map.put("SiteAddress", "http://dove.in");
            map.put("SinglePage", "Yes");
            map.put("delay", "0sec");
            json.put("builds", "http://10.207.16.9/jenkins/job/JSON-Validator/");
            QueueReference queueRef = job.build(map);
            QueueItem queueItem = js.getQueueItem(queueRef);
            if (job.isInQueue()) {
                json.put("status", "inQueue");
                json.put("buildID", queueItem.getId());
                json.put("reason", queueItem.getWhy());
                json.put("url", "http://10.207.16.9/jenkins/" + queueItem.getUrl());
                if (queueItem.isCancelled()) {
                    json.put("status", "Canceled");
                }
            }

            com.offbytwo.jenkins.model.Build build = js.getJob("JSON-Validator").getLastBuild();
            if (build.details().isBuilding()) {
                json.put("buildID", build.getNumber());
                json.put("url", build.getUrl());
                json.put("reason", "");
                json.put("status", "Executing");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ex) {
                    Logger.getLogger(Demo.class.getName()).log(Level.SEVERE, null, ex);
                }

				// json.put("info", getInfo(build.getUrl(),
                // request.getParameter("start")));
                json.put("info", getInfo(build.getUrl() + "logText/progressiveHtml", "0"));
            }

            out.print(json);
        }
    }

    private String getInfo(String url, String start) throws IOException {
        return Request.Post(url).bodyForm(Form.form().add("start", start).build()).execute().returnContent()
                .asString(Charset.defaultCharset());
    }

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on
    // the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
