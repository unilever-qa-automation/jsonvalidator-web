/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package json.validator.web;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author Sachin
 */
public class FileUploadServlet extends HttpServlet {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    // private static final long serialVersionUID = 1L;
    /**
     * @param request
     * @param response
     * @throws javax.servlet.ServletException
     * @throws java.io.IOException
     * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
     * response)
     *
     */
	// creating session Object
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            session = request.getSession();
        }
        String FileURL = "data" + File.separator + "uploadedFiles" + File.separator
                + HelperUtils.generateUniqueString();
        if (!ServletFileUpload.isMultipartContent(request)) {
            throw new IllegalArgumentException(
                    "Request is not multipart, please 'multipart/form-data' enctype for your form.");
        }
        ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
        PrintWriter writer = response.getWriter();
        response.setContentType("application/json");
        JSONArray json = new JSONArray();
        try {
            List<FileItem> items = uploadHandler.parseRequest(request);
            for (FileItem item : items) {
                if (!item.isFormField()) {
                    if (!item.getName().toLowerCase().endsWith(".properties")) {
                        throw new Exception("Invalid File Type");
                    }
                    new File(FileURL).mkdirs();
                    File file = new File(FileURL, item.getName());
                    item.write(file);
                    JSONObject jsono = new JSONObject();
                    jsono.put("path", FileURL + File.separator + item.getName());
                    json.put(jsono);
                }
            }
            writer.write(json.toString());
            writer.close();
        } catch (FileUploadException ex) {
            FileUtils.deleteQuietly(new File(FileURL));
            Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
            response.sendError(500, "Upload Failed");
        } catch (Exception ex) {
            FileUtils.deleteQuietly(new File(FileURL));
            Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
            response.sendError(500, ex.getMessage());
        }
    }
}
