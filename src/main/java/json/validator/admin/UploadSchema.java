/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package json.validator.admin;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import json.validator.web.FileUploadServlet;
import json.validator.web.HelperUtils;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FileUtils;
import org.json.JSONObject;

/**
 *
 * @author sku202
 */
public class UploadSchema extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        if (session == null) {
            session = request.getSession();
        }
        if (!session.getAttribute("Admin").equals(request.getServletContext().getInitParameter("user"))) {
            response.sendRedirect("Admin");
        }
        if (!ServletFileUpload.isMultipartContent(request)) {
            throw new IllegalArgumentException(
                    "Request is not multipart, please 'multipart/form-data' enctype for your form.");
        }

        String FileURL = "data" + File.separator + "uploadedFiles" + File.separator
                + HelperUtils.generateUniqueString();
        ServletFileUpload uploadHandler = new ServletFileUpload(new DiskFileItemFactory());
        PrintWriter writer = response.getWriter();
        response.setContentType("application/json");
        File schema = null;
        JSONObject json = new JSONObject();
        json.put("hasError", false);
        json.put("error", "");
        try {
            List<FileItem> items = uploadHandler.parseRequest(request);
            String name = getName(items);
            schema = new File(request.getServletContext().getInitParameter("schemaFolder") + File.separator + name);
            if (schema.exists()) {
                throw new Exception("Schema folder already exists with this name. Please delete it first");
            }
            schema.mkdirs();
            for (FileItem item : items) {
                if ("fileURL".equals(item.getFieldName())) {
                    if (!item.getName().toLowerCase().endsWith(".zip")) {
                        throw new Exception("Invalid File Type");
                    }
                    File f = new File(FileURL);
                    f.mkdirs();
                    File file = new File(FileURL, item.getName());
                    item.write(file);
                    HelperUtils.unzip(file, schema);
                    FileUtils.deleteQuietly(f);
                }
            }

        } catch (FileUploadException ex) {
            FileUtils.deleteQuietly(new File(FileURL));
            Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
            json.put("hasError", true);
            json.put("error", ex.getMessage());
        } catch (Exception ex) {
            FileUtils.deleteQuietly(new File(FileURL));
            Logger.getLogger(FileUploadServlet.class.getName()).log(Level.SEVERE, null, ex);
            json.put("hasError", true);
            json.put("error", ex.getMessage());
        }
        writer.print(json);
        writer.close();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private String getName(List<FileItem> items) {
        String s = "";
        for (FileItem item : items) {
            if ("id1".equals(item.getFieldName())) {
                s = item.getString();
                break;
            }
        }
        return s;
    }

}
