/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package json.validator.supporter;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author sku202
 */
public class URLInfo {

	private String url;
	private Set<String> status;
	private int pass;
	private int fail;
	private int skip;

	public URLInfo(String url) {
		this.url = url;
		status = new HashSet<>();
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 53 * hash + Objects.hashCode(this.url);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final URLInfo other = (URLInfo) obj;
		return Objects.equals(this.url, other.url);
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Set<String> getStatus() {
		return status;
	}

	public void setStatus(Set<String> status) {
		this.status = status;
	}

	public int getPass() {
		return pass;
	}

	public void updatePass() {
		this.pass++;
	}

	public int getFail() {
		return fail;
	}

	public void updateFail() {
		this.fail++;
	}

	public int getSkip() {
		return skip;
	}

	public void updateSkip() {
		this.skip++;
	}

}
