/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package json.validator.jsonservices;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

import json.validator.db.DBManager;
import json.validator.supporter.URLInfo;

/**
 *
 * @author sku202
 */
public class FetchUrls extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/javascript;charset=UTF-8");
		try (PrintWriter out = response.getWriter()) {
			String pageType = request.getParameter("pageType");
			String report = request.getParameter("report");
			String jsonp = request.getParameter("jsonp");
			BasicDBObject query = new BasicDBObject("pageType", pageType);
			JSONObject json = new JSONObject();
			JSONArray arr = new JSONArray();

			try {
				DBManager mngr = new DBManager();
				MongoDatabase db = mngr.getMongoDB();
				FindIterable<Document> find = db.getCollection(report).find(query).sort(new BasicDBObject("_id", 1));
				Set<URLInfo> logs = new HashSet<>(getURLsDetail(find));
				for (URLInfo url : logs) {
					arr.put(new JSONObject().put("url", url.getUrl()).put("status", getStatus(url.getStatus()))
							.put("fail", url.getFail()).put("pass", url.getPass()).put("skip", url.getSkip()));
				}
				json.put("rows", arr);
				mngr.close();
			} catch (Exception ex) {
				out.print("error in fetching results from DB. " + ex);
			}
			out.print(jsonp + "(" + json + ")");
		}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on
	// the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	private List<URLInfo> getURLsDetail(FindIterable<Document> find) {
		List<URLInfo> list = new ArrayList<>();
		for (Document d : find) {
			URLInfo url = new URLInfo(d.getString("url"));
			String stat = d.getString("status");
			if (list.contains(url)) {
				url = list.get(list.indexOf(url));
			}
			url.getStatus().add(stat);
			if ("PASS".equalsIgnoreCase(stat)) {
				url.updatePass();
			}
			if ("SKIP".equalsIgnoreCase(stat)) {
				url.updateSkip();
			}
			if ("FAIL".equalsIgnoreCase(stat) || "FATAL".equalsIgnoreCase(stat)) {
				url.updateFail();
			}
			list.add(url);
		}
		return list;
	}

	private String getStatus(Set<String> set) {
		if (set.contains("FATAL")) {
			return "FATAL";
		}
		if (set.contains("ERROR")) {
			return "ERROR";
		}
		if (set.contains("FAIL")) {
			return "FAIL";
		}
		if (set.contains("WARNING")) {
			return "WARNING";
		}
		if (set.contains("SKIP")) {
			return "SKIP";
		}
		if (set.contains("UNKNOWN")) {
			return "UNKNOWN";
		}
		return "PASS";
	}

}
