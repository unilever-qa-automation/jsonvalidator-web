/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package json.validator.jsonservices;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.bson.Document;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;

import json.validator.db.DBManager;

/**
 *
 * @author sku202
 */
public class getUrlsAndPageType extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/**
	 * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
	 * methods.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	protected void processRequest(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("application/javascript;charset=UTF-8");
		try (PrintWriter out = response.getWriter()) {
			String report = request.getParameter("report");
			String jsonp = request.getParameter("jsonp");
			JSONObject json = new JSONObject();
			JSONArray arr = new JSONArray();

			try {
				DBManager mngr = new DBManager();
				MongoDatabase db = mngr.getMongoDB();
				FindIterable<Document> find = db.getCollection(report).find();
				Map<String, Set<String>> logs = getURLs(find);
				for (String pageType : logs.keySet()) {
					arr.put(new JSONObject().put("pageType", pageType).put("urls", logs.get(pageType).size()));
				}
				json.put("rows", arr);
				mngr.close();
			} catch (Exception ex) {
				out.print("error in fetching results from DB. " + ex);
			}
			out.print(jsonp + "(" + json + ")");
		}
	}

	// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on
	// the + sign on the left to edit the code.">
	/**
	 * Handles the HTTP <code>GET</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Handles the HTTP <code>POST</code> method.
	 *
	 * @param request
	 *            servlet request
	 * @param response
	 *            servlet response
	 * @throws ServletException
	 *             if a servlet-specific error occurs
	 * @throws IOException
	 *             if an I/O error occurs
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * Returns a short description of the servlet.
	 *
	 * @return a String containing servlet description
	 */
	@Override
	public String getServletInfo() {
		return "Short description";
	}// </editor-fold>

	private Map<String, Set<String>> getURLs(FindIterable<Document> find) {
		Map<String, Set<String>> map = new HashMap<>();
		for (Document d : find) {
			String stat = d.getString("url");
			String key = d.getString("pageType");
			if (!map.containsKey(key)) {
				Set<String> status = new HashSet<>();
				status.add(stat);
				map.put(key, status);
			} else {
				Set<String> status = map.get(key);
				status.add(stat);
				map.put(key, status);
			}
		}
		return map;
	}

}
